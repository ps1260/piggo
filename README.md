# PigGo

The Pig Latin converter written in Go that nobody wants or needs. To use, just give it a filepath to a text file as an argument.
