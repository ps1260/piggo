// pig
package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	args := os.Args
	if len(args) == 1 {
		fmt.Println("pig latin... so useless... but maybe fun... i can haz filepath plz?")
		return
	}
	filename := args[1]
	linearr := loadfile(filename)
	for i, _ := range linearr {
		linearr[i] = otay_igpay_atinlay(linearr[i])
	}
	writefile(filename+".pig",linearr)
}

func loadfile(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		panic("Bad filename give")
	}
	defer file.Close()
	var linearr []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		linearr = append(linearr, scanner.Text())
	}
	return linearr
}

func writefile(filename string,linearr []string) error {
	file, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	for _, line := range linearr {
		fmt.Fprintln(w, line)
	}
	return w.Flush()
}

func otay_igpay_atinlay(in string) (out string) {
	prefix := ""
	is_word := false
	is_prefix := false
	for _, chr := range in {
		if is_word {
			if is_prefix {
				if is_letter(chr) {
					if is_vowel(chr) {
						out = out + string(chr)
						is_prefix = false
					} else {
						prefix = prefix + string(chr)
					}
				} else {
					if prefix == "" {
						out = out + "way"
					} else {
						out = out + prefix + "ay" + string(chr)
					}
					is_word = false
				}
			} else {
				if is_letter(chr) {
					out = out + string(chr)
				} else {
					if prefix == "" {
						out = out + "way"
					} else {
						out = out + prefix + "ay" + string(chr)
					}
					is_word = false
				}
			}
		} else {
			if is_letter(chr) {
				if is_vowel(chr) {
					out = out + string(chr)
				} else {
					prefix = string(chr)
					is_prefix = true
				}
				is_word = true
			} else {
				out = out + string(chr)
			}
		}
	}
	if is_word {
		if prefix == "" {
			out = out + "way"
		} else {
			out = out + prefix + "ay"
		}
	}
	return
}

func is_vowel(in rune) bool {
	return strings.ContainsAny(string(in), "aeiouAEIOU")
}
func is_letter(in rune) bool {
	return ('a' <= in && in <= 'z') || ('A' <= in && in <= 'Z')
}
